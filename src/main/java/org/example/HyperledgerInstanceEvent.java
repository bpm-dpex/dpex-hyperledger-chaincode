package org.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

import java.util.HashMap;
import java.util.Map;

@DataType
public class HyperledgerInstanceEvent {
    @Property
    private final String globalInstanceReference;

    @Property
    private final String JSONVariables;

    public HyperledgerInstanceEvent(@JsonProperty("globalInstanceReference") final String globalInstanceReference,
                                    @JsonProperty("JSONVariables") final String JSONVariables) {
        this.globalInstanceReference = globalInstanceReference;
        this.JSONVariables = JSONVariables;
    }

    public String serialize() {
        Map<String, String> map = new HashMap<>();
        map.put("globalInstanceReference", globalInstanceReference);
        map.put("variables", JSONVariables);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static HyperledgerInstanceEvent deserialize (final String eventJSON) {

        ObjectMapper mapper = new ObjectMapper();

        try {
            HashMap<String, String> map = mapper.readValue(eventJSON, HashMap.class);
            return new HyperledgerInstanceEvent(map.get("globalInstanceReference"), map.get("variables"));

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}
