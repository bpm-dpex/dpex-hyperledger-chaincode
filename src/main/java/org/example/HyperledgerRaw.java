package org.example;

import com.google.common.hash.Hashing;
import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.*;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.nio.charset.StandardCharsets;

@Contract(
        name = "HyperledgerSCI",
        info = @Info(
                title = "Hyperledger-DPEX",
                description = "Contract to emit events to manage task in decentralized processes",
                version = "0.0.1-SNAPSHOT",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "tobias.gretsch@uni-bayreuth.de")
        )
)

@Default
public final class HyperledgerRaw implements ContractInterface {


    @Transaction
    public void taskAction(final Context ctx,
                           final String globalInstanceReference,
                           final String activity,
                           final String stage,
                           final String resource,
                           final String variables) {

        ChaincodeStub stub = ctx.getStub();
        HyperledgerEvent event = new HyperledgerEvent(globalInstanceReference, activity, stage,  resource, variables);

        stub.setEvent("taskAction", event.serialize().getBytes());

    }

    @Transaction
    public void instantiate(final Context ctx,
                            final String JSONVariables) {
        ChaincodeStub stub = ctx.getStub();

        String InstanceReference = "hl" + Hashing.sha256()
                .hashString(stub.getTxTimestamp().toString(), StandardCharsets.UTF_8)
                .toString();

        HyperledgerInstanceEvent instanceEvent = new HyperledgerInstanceEvent(InstanceReference, JSONVariables);

        stub.setEvent("instantiate", instanceEvent.serialize().getBytes());
    }


}