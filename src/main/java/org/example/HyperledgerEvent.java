package org.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.j2objc.annotations.Property;
import org.hyperledger.fabric.contract.annotation.DataType;

import java.util.HashMap;
import java.util.Map;

@DataType
public final class HyperledgerEvent {

    @Property
    private final String globalInstanceReference;

    @Property
    private final String activity;

    @Property
    private final String stage;

    @Property
    private final String resource;

    @Property
    private final String variables;

    public String getGlobalInstanceReference() {
        return globalInstanceReference;
    }

    public String getResource() {
        return resource;
    }

    public HyperledgerEvent(@JsonProperty("globalInstanceReference") final String globalInstanceReference,
                            @JsonProperty("activity") final String activity,
                            @JsonProperty("stage") final String stage,
                            @JsonProperty("resource") final String resource,
                            @JsonProperty("variables") final String variables) {
        this.globalInstanceReference = globalInstanceReference;
        this.activity = activity;
        this.stage = stage;
        this.resource = resource;
        this.variables = variables;
    }

    public String serialize() {
        Map<String, String> map = new HashMap<>();
        map.put("globalInstanceReference", globalInstanceReference);
        map.put("activity", activity);
        map.put("stage", stage);
        map.put("resource", resource);
        map.put("variables", variables);

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static HyperledgerEvent deserialize(final String eventJSON) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            HashMap<String, String> map = mapper.readValue(eventJSON, HashMap.class);
            return new HyperledgerEvent(map.get("globalInstanceReference"), map.get("activity"),map.get("stage"), map.get("resource"), map.get("variables"));

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}